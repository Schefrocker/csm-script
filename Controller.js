/**
 * Serves as a collection of all registered features.
 * Upon each user's request, the controller will
 * init all registered features, by delegating the
 * handling to each feature's own implementation.
 * <p>
 * A single controller instance is hereby provided
 * upon @require command within main.js,
 * in order for each feature impl. to register itself.
 *
 * @constructor
 * @class
 */
Controller = function () {

    this.features = {};

    this.init = function () {
        for (var key in this.features) {
            this.features[key].handle();
        }
    };

};

Controller.prototype = {

    registerFeature: function (key, feature) {
        this.features[key] = feature;
    }

};

var controller = new Controller();

