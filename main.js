// ==UserScript==
// @name            CSM-Script
// @namespace       CSM-Script
// @description     Does some things, CSM should do anyway
// @include         http://www.cs-manager.com/*
// @version         2.0
// @author          schefrocker
// @downloadURL     https://bitbucket.org/Schefrocker/csm-script/raw/master/main.js
// @updateURL       https://bitbucket.org/Schefrocker/csm-script/raw/master/main.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.2/babel.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.16.0/polyfill.js
// @require         https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require         https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js
// @require         https://userscripts-mirror.org/scripts/source/107941.user.js
// @require         https://bitbucket.org/Schefrocker/csm-script/raw/master/Controller.js
// @require         https://bitbucket.org/Schefrocker/csm-script/raw/master/util/Builder.js
// @require         https://bitbucket.org/Schefrocker/csm-script/raw/master/feature/transferlist/TransferList.js
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_addStyle
// @grant           GM_deleteValue
// @grant           GM_getResourceText
// @grant           GM_log
// @grant           GM_listValues
// ==/UserScript==

/* jshint ignore:start */
var inline_src = (<><![CDATA[
        /* jshint ignore:end */
        /* jshint esnext: false */
        /* jshint esversion: 6 */

        /*#######################################################*/
        /* ----- FILE NEEDS TO SET AS SCRIPT IN TAMPERMONKEY-----*/
        /*#######################################################*/
        if (window.location.href.indexOf("http://www.cs-manager.com/?l=") === -1) {
            controller.init();
            var currentCSMDay = document.getElementById("date-day").children[0].innerHTML;
        }else {
            //No need to support the old CSM-URL
        }

/* jshint ignore:start */
]]></>).toString();
var c = Babel.transform(inline_src, { presets: [ "es2015", "es2016" ] });
eval(c.code);
/* jshint ignore:end */