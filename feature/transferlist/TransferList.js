const TRANSFER_LIST_KEY = "transferList";

const TRANSFER_LIST_SETTINGS_KEY = "transferListSettingsKey";

TransferList = function () {

    this.myTransferListSettings = undefined;

    this.handle = function () {
        /*Transfer List settings execute*/
        if (window.location.href.indexOf("office_transfer") !== -1 &&
            window.location.href.indexOf("office_transfer&s=active") === -1) {

            this.init();
        }
    };

    this.init = function () {
        /*include custom style*/
        Builder.addCss(transferListSelectorCss);
        this.loadSettings();
        this.includeSelectorHTML(this.myTransferListSettings);
        this.initializeMenuSettings(this);
        this.initializeSelectorValues(this);
        this.handleNonMatchingPlayers(this.myTransferListSettings);
    };

    this.loadSettings = function () {
        this.myTransferListSettings = GM_SuperValue.get(TRANSFER_LIST_SETTINGS_KEY, undefined);
        if (!this.myTransferListSettings || this.myTransferListSettings == undefined) {
            this.myTransferListSettings = new TransferListLimitSettingsModel();
        }
        GM_log("CSM-SCRIPT -> TRANSFER_LIST_SETTINGS: " + JSON.stringify(this.myTransferListSettings));
    };


    this.includeSelectorHTML = function (settings) {
        let margin = settings.toggle == "open-menu" ? "0px" : "-170px";
        $('header').after($('\
    <div id="limitselector_container" class="' + settings.toggle + '" style="margin-left: ' + margin + ';"> \
        <div id="limitselector_header"><ul><li id="limitselector_status" class="' + settings.status + '"></li><li id="limitselector_toggle" class="' + settings.toggle + '"><a href="javascript:void(0);"></a></li></ul></div>\
        <div id="limitselector_content">  \
            <label for="amountaim">Aim:</label> \
            <input type="text" id="amountaim" readonly style="border: none;"> \
            <div id="slideraim"></div> \
            \
            <label for="amountteamplay">Teamplay:</label> \
            <input type="text" id="amountteamplay" readonly style="border: none;"> \
            <div id="sliderteamplay"></div> \
            \
            <label for="amounthandling">Handling:</label> \
            <input type="text" id="amounthandling" readonly style="border: none;"> \
            <div id="sliderhandling"></div> \
            \
            <label for="amountplayingiq">Playing IQ:</label> \
            <input type="text" id="amountplayingiq" readonly style="border: none;"> \
            <div id="sliderplayingiq"></div> \
            \
            <label for="amountquickness">Quickness:</label> \
            <input type="text" id="amountquickness" readonly style="border: none;"> \
            <div id="sliderquickness"></div> \
            \
            <label for="amountdetermination">Determination:</label> \
            <input type="text" id="amountdetermination" readonly style="border: none;"> \
            <div id="sliderdetermination"></div> \
            \
            <label for="amountawareness">Awareness:</label> \
            <input type="text" id="amountawareness" readonly style="border: none;"> \
            <div id="sliderawareness"></div> \
            \
            <label for="amountcreativity">Creativity:</label> \
            <input type="text" id="amountcreativity" readonly style="border: none;"> \
            <div id="slidercreativity"></div> \
            \
            <label for="amountpatience">Patience:</label> \
            <input type="text" id="amountpatience" readonly style="border: none;"> \
            <div id="sliderpatience"></div> \
            \
            <label for="amountcalmness">Calmness:</label> \
            <input type="text" id="amountcalmness" readonly style="border: none;"> \
            <div id="slidercalmness"></div> \
        </div> \
    </div> \
    '));
        let toggleState = settings.toggle == 'open-menu' ? LIMIT_SELECTOR_HTML_OPEN : LIMIT_SELECTOR_HTML_CLOSE;
        $('#limitselector_toggle a').html(toggleState);
    };

    this.initializeMenuSettings = function (transferListModel) {
        $('#limitselector_status').click(function () {
            $(this).toggleClass(transferListModel.myTransferListSettings.status);
            transferListModel.myTransferListSettings.status = transferListModel.myTransferListSettings.status == "on" ? "off" : "on";
            GM_SuperValue.set(TRANSFER_LIST_SETTINGS_KEY, transferListModel.myTransferListSettings);
            $(this).toggleClass(transferListModel.myTransferListSettings.status);
            transferListModel.handleNonMatchingPlayers(transferListModel.myTransferListSettings);
        });

        $('#limitselector_toggle').click(function () {
            $('#limitselector_container').toggleClass(transferListModel.myTransferListSettings.toggle);
            transferListModel.myTransferListSettings.toggle = transferListModel.myTransferListSettings.toggle == "open-menu" ? "close-menu" : "open-menu";
            GM_SuperValue.set(TRANSFER_LIST_SETTINGS_KEY, transferListModel.myTransferListSettings);
            let htmlValue = transferListModel.myTransferListSettings.toggle == "open-menu" ? LIMIT_SELECTOR_HTML_OPEN : LIMIT_SELECTOR_HTML_CLOSE;
            $('#limitselector_toggle a').html(htmlValue);
            if (transferListModel.myTransferListSettings.toggle == "open-menu") {
                $('#limitselector_container').animate({
                    marginLeft: "0px",
                }, 500);
            } else {
                $('#limitselector_container').animate({
                    marginLeft: "-170px",
                }, 500);
            }
            $('#limitselector_container').toggleClass(transferListModel.myTransferListSettings.toggle);
        });
    };

    this.initializeSelectorValues = function (transferListModel) {
        this.setSelectorValue(transferListModel, "#slideraim", this.myTransferListSettings.limit_aim, "#amountaim");
        this.setSelectorValue(transferListModel, "#sliderteamplay", this.myTransferListSettings.limit_teamplay, "#amountteamplay");
        this.setSelectorValue(transferListModel, "#sliderhandling", this.myTransferListSettings.limit_handling, "#amounthandling");
        this.setSelectorValue(transferListModel, "#sliderplayingiq", this.myTransferListSettings.limit_playingiq, "#amountplayingiq");
        this.setSelectorValue(transferListModel, "#sliderquickness", this.myTransferListSettings.limit_quickness, "#amountquickness");
        this.setSelectorValue(transferListModel, "#sliderdetermination", this.myTransferListSettings.limit_determination, "#amountdetermination");
        this.setSelectorValue(transferListModel, "#sliderawareness", this.myTransferListSettings.limit_awareness, "#amountawareness");
        this.setSelectorValue(transferListModel, "#slidercreativity", this.myTransferListSettings.limit_creativity, "#amountcreativity");
        this.setSelectorValue(transferListModel, "#sliderpatience", this.myTransferListSettings.limit_patience, "#amountpatience");
        this.setSelectorValue(transferListModel, "#slidercalmness", this.myTransferListSettings.limit_calmness, "#amountcalmness");
    };

    this.setSelectorValue = function (transferListModel, sliderID, sliderValue, amountID) {
        $(sliderID).slider({
            range: "min",
            min: 70,
            max: 100,
            value: sliderValue,
            orientation: "horizontal",
            animate: true,
            step: 1,
            slide: function (event, ui) {
                $(amountID).val(ui.value);
                transferListModel.saveSelectedValue(sliderID, ui.value);
                transferListModel.handleNonMatchingPlayers(transferListModel.myTransferListSettings);
            }
        });
        $(amountID).val($(sliderID).slider("value"));
    };

    this.saveSelectedValue = function (sliderID, value) {
        if (sliderID === '#slideraim') {
            this.myTransferListSettings.limit_aim = value;
        }
        if (sliderID === '#sliderteamplay') {
            this.myTransferListSettings.limit_teamplay = value;
        }
        if (sliderID === '#sliderhandling') {
            this.myTransferListSettings.limit_handling = value;
        }
        if (sliderID === '#sliderplayingiq') {
            this.myTransferListSettings.limit_playingiq = value;
        }
        if (sliderID === '#sliderquickness') {
            this.myTransferListSettings.limit_quickness = value;
        }
        if (sliderID === '#sliderdetermination') {
            this.myTransferListSettings.limit_determination = value;
        }
        if (sliderID === '#sliderawareness') {
            this.myTransferListSettings.limit_awareness = value;
        }
        if (sliderID === '#slidercreativity') {
            this.myTransferListSettings.limit_creativity = value;
        }
        if (sliderID === '#sliderpatience') {
            this.myTransferListSettings.limit_patience = value;
        }
        if (sliderID === '#slidercalmness') {
            this.myTransferListSettings.limit_calmness = value;
        }
        GM_SuperValue.set(TRANSFER_LIST_SETTINGS_KEY, this.myTransferListSettings);
    };

    this.handleNonMatchingPlayers = function (settings) {
        if (settings.status == "off") {
            $('article').each(function (i, val) {
                $(this).show();
            });
        } else {
            $('article').each(function (i, val) {
                let hidePlayer = false;
                $('.skills-bar', $(this)).each(function () {
                    $(this).find('li').each(function (i, val) {
                        let limit = TransferList.prototype.decipherLimitFromSrc($(this));
                        if (limit > "0") {
                            switch (i) {
                                case 0:
                                    if (limit < settings.limit_aim) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 1:
                                    if (limit < settings.limit_teamplay) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 2:
                                    if (limit < settings.limit_handling) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 3:
                                    if (limit < settings.limit_playingiq) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 4:
                                    if (limit < settings.limit_quickness) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 5:
                                    if (limit < settings.limit_determination) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 6:
                                    if (limit < settings.limit_awareness) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 7:
                                    if (limit < settings.limit_creativity) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 8:
                                    if (limit < settings.limit_patience) {
                                        hidePlayer = true;
                                    }
                                    break;
                                case 9:
                                    if (limit < settings.limit_calmness) {
                                        hidePlayer = true;
                                    }
                                    break;
                                default:
                                // do nothing
                            }
                        }
                    })
                });
                if (hidePlayer && $(this).is(':visible')) {
                    $(this).hide();
                    GM_log("CSM-SCRIPT -> Hide player: " + $(this).find(".player-nick a")[0].text);
                }
                if (!hidePlayer && !$(this).is(':visible')) {
                    $(this).show();
                    GM_log("CSM-SCRIPT -> Show player: " + $(this).find(".player-nick a")[0].text);
                }
            })
        }
    };

    TransferList.prototype = {

        decipherLimitFromSrc: function (srcElement) {
            let src = $('img', srcElement).attr('src');
            let limit = src.match(/limit=\d+/)[0].split('=').pop();
            return limit;
        }

    };

};

TransferListLimitSettingsModel = function () {
    this.toggle = "open-menu";
    this.status = "on";
    this.limit_aim = 70;
    this.limit_teamplay = 70;
    this.limit_handling = 70;
    this.limit_playingiq = 70;
    this.limit_quickness = 70;
    this.limit_determination = 70;
    this.limit_awareness = 70;
    this.limit_creativity = 70;
    this.limit_patience = 70;
    this.limit_calmness = 70;
};


let transferList = new TransferList();
controller.registerFeature(TRANSFER_LIST_KEY, transferList);

const LIMIT_SELECTOR_HTML_OPEN = " « ";

const LIMIT_SELECTOR_HTML_CLOSE = " » ";

const LIMIT_SELECTOR_IMAGE_ON = "/images/csm/buddy-list-online.png";

const LIMIT_SELECTOR_IMAGE_OFF = "/images/csm/buddy-list-offline.png";

const transferListSelectorCss = '' +
    '#limitselector_container       { width: 200px; height: 420px; position: sticky; top: 25%; z-index: 4; background: #fff;' +
    ' box-shadow: 0px 0px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.16); border-top-right-radius: 5px; border-bottom-right-radius: 5px; }' +
    '#limitselector_content         { width: 150px; margin-left: 20px; }' +
    '#limitselector_content input   { width: 30px; background-color: transparent; font-weight: bold; font-size: 11px; font-family: verdana, sans-serif;}' +
    '#limitselector_content label   { display: inline-block; width: 95px; padding: 10px 5px 0px 10px; }' +
    '#container                     { margin-top: -390px; }' +
    '#limitselector_header          { background: #f4f4f4; box-shadow: 0 1px #fff inset, 0 -1px #ddd inset; border-top-right-radius: 5px; }' +
    '#limitselector_header ul       { display: inline-flex; margin: 5px 0px 5px 157px; padding: 0px; }' +
    '#limitselector_header ul li    { list-style-type: none; padding-right: 23px; font-size: 20px; }' +
    '#limitselector_status.on       { list-style-image: url(' + LIMIT_SELECTOR_IMAGE_ON + '); line-height: 16px; }' +
    '#limitselector_status.off      { list-style-image: url(' + LIMIT_SELECTOR_IMAGE_OFF + '); line-height: 16px; }';