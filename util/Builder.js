/**
 * Base utility to build gui elements & extensions
 *
 * @static
 *
 * @class
 */
Builder = {

    /**
     * Appends a style element to head html,
     * in order to add CSS styling to your custom gui element.
     *
     * @static
     *
     * @type {function}
     *
     * @param {string} cssString
     */
    addCss: function (cssString) {
        let head = document.getElementsByTagName('head')[0];
        let newCss = document.createElement('style');
        newCss.type = "text/css";
        newCss.innerHTML = cssString;
        head.appendChild(newCss);
    }

};

